import os
import re
# import matplotlib.pyplot as plt 
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import time
import subprocess
import pandas as pd



lp=0
distance_list=[]
sweep_list=[]
difference_list=[]
data_list=[]
angle_list=[]
data_points = 0
count=0
sweep=False
sweep_marker=""


#This is an additional line

#this is a new feature

#adjust when needed(in minutes)
#interval_time=10
# interval_time = interval_time*1000

#intervals=3 #Number of  

data_dict = {"THETA": 0, "DIST":0 }
output_df0 = pd.DataFrame()
output_df1 = pd.DataFrame()
output_df2 = pd.DataFrame()
output_df3 = pd.DataFrame()
output_df4 = pd.DataFrame()
final_df=pd.DataFrame()
diff_2 = 0
theta_list=[]

elapsed_time =0

duration =3600

interval_time=300









start = time.time()
# for x in range(0, intervals):
while elapsed_time < duration:
    os.system('python serial_2.py')#collects data from rp_lidar and saves to file
    print("Writing to file...")
    time.sleep(4) #delay for writing to a  file
    print("Finished writing to file...")

    #READING THE  FILE THAT WAS WRITTEN
    with open('data_file.txt') as f:
        lines=f.readlines()
        print(len(lines))
        




        for index, x in enumerate(lines):
            # print(index, type(x)
            
            if "theta" in x:
                data_packet=x.split()
                theta = data_packet[1]
                data_list.append(data_packet)

        time.sleep(3)

            

            

        for x in data_list:
            # print(x)

            try:
                distance=x[3]
                # print(distance)
                theta = x[1]
            
                theta_2= float(theta.strip())

            except:
                pass

        

            

            if theta_2 >= 120 and theta_2 <= 240:
                diff= 240-theta_2
                # print("THETA", theta_2, "DIST", distance )
                sweep_list.append(theta_2)

            

                if(diff < 0.5):
                    # print(count, "th SWEEP DETECTED!")
                    sweep_marker=str(count)+"s"
                    print(sweep_marker)
                    count=count+1

        
                if sweep_marker == "0s":
                    data_dict["THETA"] = theta_2
                    data_dict["DIST"] = distance
                    output_df0 = output_df0.append(data_dict, ignore_index=True)


                elif sweep_marker == "1s":
                    data_dict["THETA"] = theta_2
                    data_dict["DIST"] = distance
                    output_df1 = output_df1.append(data_dict, ignore_index=True)

                elif sweep_marker == "2s":
                    data_dict["THETA"] = theta_2
                    data_dict["DIST"] = distance
                    output_df2 = output_df2.append(data_dict, ignore_index=True)

                elif sweep_marker == "4s":
                    data_dict["THETA"] = theta_2
                    data_dict["DIST"] = distance
                    output_df3 = output_df3.append(data_dict, ignore_index=True)

                elif sweep_marker == "5s":
                    data_dict["THETA"] = theta_2
                    data_dict["DIST"] = distance
                    output_df4 = output_df4.append(data_dict, ignore_index=True)
                    


                


        # print(data_dict)
        # print(sweep_list)
        count=0
        # print(output_df0)
        # print(output_df1)

        try:


            deg_list0 = output_df0["THETA"].values.tolist()
            dist_list0 = output_df0["DIST"].values.tolist()

            deg_list1 = output_df1["THETA"].values.tolist()
            dist_list1 = output_df1["DIST"].values.tolist()

            deg_list2 = output_df2["THETA"].values.tolist()
            dist_list2 = output_df2["DIST"].values.tolist()

            deg_list3 = output_df3["THETA"].values.tolist()
            dist_list3 = output_df3["DIST"].values.tolist()

            deg_list4 = output_df4["THETA"].values.tolist()
            dist_list4 = output_df4["DIST"].values.tolist()
        except:
            pass





        final_df["DIST0"] = pd.Series(dist_list0)
        final_df["DIST1"] = pd.Series(dist_list1)
        final_df["DIST2"] = pd.Series(dist_list2)
        final_df["DIST3"] = pd.Series(dist_list3)
        final_df["DIST4"] = pd.Series(dist_list4)


        leng0=len(dist_list0)
        leng1= len(dist_list1)
        leng2=len(dist_list2)
        leng3=len(dist_list3)
        leng4=len(dist_list4)



        print("SIZE OF THE LISTS")
        print(leng0)
        print(leng1)
        print(leng2)
        print(leng3)
        print(leng4)



        lp = lp +1
        print("TIMES:", lp)

        end= time.time()
        elapsed_time = end - start
    
        #PLOTTING
    plt_0 = plt.figure(figsize=(6, 3))
    plt.plot(dist_list3, color='magenta', marker='o',mfc='pink' ) #plot the data
    plt.xticks(range(-1,len(dist_list4), 5)) #set the tick frequency on x-axis
    plt.ylabel('dist') #set the label for y axis
    plt.xlabel('index') #set the label for x-axis
    plt.title("Plot for one sweep") #set the title of the graph
    # plt.show() #display the graphp./
    script_dir = os.path.abspath(__file__) # Figures out the absolute path for you in case your working directory moves around.
    my_file_name='graph'+str(lp)+'.png'
    plt.savefig(my_file_name)  
    time.sleep(interval_time)

    

    print("RESET DATAFRAMES")
    output_df0 = pd.DataFrame()
    output_df1 = pd.DataFrame()
    output_df2 = pd.DataFrame()
    output_df3 = pd.DataFrame()
    output_df4 = pd.DataFrame()

     #Reseting the data file after each time intervale
    os.remove('data_file.txt')
    os.path.isfile('data_file.txt')

    if os.path.exists('./test.txt') :
        print("FILE DELETED!")

    

lp=0
print("elapsed time", elapsed_time)
print("TIME'S UP")

#END OF THE LOOP




        